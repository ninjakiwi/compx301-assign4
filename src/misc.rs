//! Library for all block stacking related things.
//! 
//! # Authors
//! 
//!  - Daniel Martin - 1349779
//!  - Rex Pan - 1318909
//! 
//! ## Structs:
//! 
//!  - `StackedBox` - A stacked box, contains a `StackBox` for box identification and size and a Orientation.
//!  - `StackBox` - A box that could be stacked, contains dimensions and a id.
//!  - `Face` - Dimensions of a face.
//!  - `Tower` - A stack of stacked blocks.
//!  - `RNG` - Provides seemingly random numbers from a seed.
//! 
//! ## Enums:
//! 
//!  - `Orientation` - Specifies which axis is up.
//!  - `Fit` - Ways a block can fit on another.
//!  - `SendData` - Types of data that can be sent between threads.

use std::cmp::Ordering;
use std::collections::HashSet;
use std::hash::{Hash, Hasher};

/// A block containing dimensions and id.
#[derive(Debug, Clone)]
struct StackBox {
	id: usize,
	x: i32,
	y: i32,
	z: i32,
}

/// Description of a stacked block, a `StackBox` but with an orientation.
#[derive(Debug, Clone)]
struct StackedBox {
	box_size: StackBox,
	orientation: Orientation,
}

impl StackedBox {
	fn height(&self) -> i32 {
		match &self.orientation {
			Orientation::X => self.box_size.x,
			Orientation::Y => self.box_size.y,
			Orientation::Z => self.box_size.z,
		}
	}

	fn face(&self) -> Face {
		match &self.orientation {
			Orientation::X => Face(self.box_size.z, self.box_size.y),
			Orientation::Y => Face(self.box_size.x, self.box_size.z),
			Orientation::Z => Face(self.box_size.x, self.box_size.y),
		}
	}

	fn formatted(&self, is_rot: bool) -> String {
		let ori = match &self.orientation {
			Orientation::X => (self.box_size.z, self.box_size.y, self.box_size.x),
			Orientation::Y => (self.box_size.x, self.box_size.z, self.box_size.y),
			Orientation::Z => (self.box_size.x, self.box_size.y, self.box_size.z),
		};

		// Change length and width depending on rotation
		if is_rot {
			format!("{} {} {}", ori.0, ori.1, ori.2)
		} else {
			format!("{} {} {}", ori.1, ori.0, ori.2)
		}
	}

	fn box_id(&self) -> usize {
		self.box_size.id
	}
}

impl PartialEq for StackedBox {
	fn eq(&self, other: &Self) -> bool {
		self.box_size.id == other.box_size.id && self.orientation == other.orientation
	}
}

impl Eq for StackedBox {}

impl Hash for StackedBox {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.box_size.id.hash(state);
	}
}

/// Orientation of a block
#[derive(Debug, Clone, Eq, PartialEq)]
enum Orientation {
	X,
	Y,
	Z,
}

impl Orientation {
	fn random_orientation(rng: &mut RNG) -> Orientation {
		match rng.next_range(0, 3) {
			0 => Orientation::X,
			1 => Orientation::Y,
			2 => Orientation::Z,
			_ => unreachable!("RNG range goes out of range")
		}
	}
}

/// Ways two blocks could stack
#[derive(Debug, Eq, PartialEq)]
enum Fit {
	Above,
	Below,
	Neither,
}

/// A face, two values for dimensions.
#[derive(Debug)]
struct Face(i32, i32);

impl Face {
	/// Whether it fits and if it's rotated
	fn does_self_fit(&self, other: &Self) -> (Fit, bool) {
		let comp_match_0 = self.0.cmp(&other.0);
		let comp_match_1 = self.1.cmp(&other.1);

		// Can't fit flush
		if comp_match_0 == Ordering::Equal || comp_match_1 == Ordering::Equal {
			return (Fit::Neither, false);
		}

		// Both comps same, one block must fit on the other because we checked they weren't equal
		if comp_match_0 == comp_match_1 {
			return match comp_match_0 {
				Ordering::Greater => (Fit::Below, false),
				Ordering::Equal => unreachable!(),
				Ordering::Less => (Fit::Above, false)
			};
		}

		// Check if it fits rotated
		let comp_cross_0 = self.0.cmp(&other.1);
		let comp_cross_1 = self.1.cmp(&other.0);

		// Can't fit flush
		if comp_cross_0 == Ordering::Equal || comp_cross_1 == Ordering::Equal {
			return (Fit::Neither, false);
		}

		// Comps same, one must fit on the other (check for equality)
		if comp_cross_0 == comp_cross_1 {
			return match comp_cross_0 {
				Ordering::Greater => (Fit::Below, true),
				Ordering::Equal => unreachable!(),
				Ordering::Less => (Fit::Above, true)
			};
		}

		// No fit
		(Fit::Neither, false)
	}
}

/// A `Tower` builder so we don't have to expose StackedBox or StackBox
pub struct TowerBuilder(Vec<StackedBox>);

impl TowerBuilder {
	pub fn new() -> TowerBuilder {
		TowerBuilder(Vec::new())
	}

	/// Push a box (from a str) to the vector
	pub fn push_from_string(&mut self, data: String) -> Result<(), ()> {
		let mut box_stuff = data
			.split_whitespace()
			.map(|thing| thing.parse::<i32>().ok());

		// Get dimensions, first or_or_else is for the iterator, second for the parsing.
		let stack_box = StackBox {
			id: self.0.len(),
			x: box_stuff.next().ok_or_else(|| ())?.ok_or_else(|| ())?,
			y: box_stuff.next().ok_or_else(|| ())?.ok_or_else(|| ())?,
			z: box_stuff.next().ok_or_else(|| ())?.ok_or_else(|| ())?,
		};

		// Check all values are greater then or = to 0
		if stack_box.x <= 0 || stack_box.y <= 0 || stack_box.z <= 0 {
			return Err(());
		}

		// Push the new box to our vector and return
		self.0.push(StackedBox {
			box_size: stack_box,
			orientation: Orientation::Z,
		});
		Ok(())
	}

	/// Consume the builder and produce a tower
	pub fn build(self) -> Tower {
		Tower::new(self.0)
	}
}

/// A tower of blocks keeps track of height after changes.
#[derive(Debug, Clone)]
pub struct Tower {
	boxes: Vec<StackedBox>,
	height: usize,
}

impl Tower {
	/// Private new function, all towers should be made via the builder or cloned from an existing one
	fn new(boxes: Vec<StackedBox>) -> Tower {
		let mut out = Tower { height: 0, boxes };
		out.update_height();
		out
	}

	/// Compare the height of two towers
	pub fn sound_compare(a: &Tower, b: &Tower) -> Ordering {
		a.height()
			.cmp(&b.height())
			.reverse()
	}

	/// Create a new (shuffled) tower using the current tower's boxes
	pub fn clone_shuffled(&self, rng: &mut RNG) -> Tower {
		let mut data = self.clone();
		data.shuffle(1.0, 0.0, rng);
		data.update_height();
		data
	}

	/// Dumps the blocks in this tower followed by the height at each stage
	pub fn dump(&self) {
		// Current rotation of the tower, used so we can line up x and y when printing
		let mut curr_rot = false;

		self.boxes.iter()
			.fold((Face(std::i32::MAX, std::i32::MAX), 0),
			      |(last_face, height), c| {
				      let new_face = c.face();
				      let fit = last_face.does_self_fit(&new_face);

				      // If box fits on tower
				      if fit.0 == Fit::Below {
					      let new_height = height + c.height();
					      curr_rot = curr_rot != fit.1;

					      println!("{} {}", c.formatted(curr_rot), new_height);

					      (new_face, new_height)
				      } else {
					      // Ignore box (doesn't fit)
					      (last_face, height)
				      }
			      });
	}

	/// Gets calculated height
	pub fn height(&self) -> usize {
		self.height
	}

	/// Merge the tower with another
	pub fn merge(&self, other_tower: &Tower, rng: &mut RNG) -> Tower {
		// Vec for new tower and hash set for checking for duplicates
		let mut data = Vec::with_capacity(self.num_of_boxes());
		let mut set = HashSet::new();

		// For each level of the tower
		for (b1, b2) in self.boxes.iter().zip(other_tower.boxes.iter()) {
			// Randomly pick one of the two towers to insert that box
			// (push `None` and well fill later if the picked box is already in the tower)
			if rng.next() % 2 == 0 {
				if set.insert(b1.box_id()) {
					data.push(Some(b1.clone()));
				} else {
					data.push(None)
				}
			} else {
				if set.insert(b2.box_id()) {
					data.push(Some(b2.clone()));
				} else {
					data.push(None)
				}
			};
		}

		// Find the remaining boxes that didn't fit
		let mut remaining = self.boxes.iter().filter_map(|c| {
			if set.insert(c.box_id()) {
				let out = c.clone();
				Some(out)
			} else {
				None
			}
		});

		// Create and return the new tower, inserting any missing blocks (new constructor will calculate the height automatically)
		Tower::new(data.into_iter().map(|c| if let Some(c) = c {
			c
		} else {
			remaining.next().expect("No more items")
		}).collect())
	}

	/// Shuffle the tower's block in location and rotation, favour_top make it more likely to leave higher up boxes.
	/// Set favour top to 0 to have chance_to_change equal for all towers.
	pub fn shuffle(&mut self, chance_to_change: f64, favour_top: f64, rng: &mut RNG) {
		let box_length = self.num_of_boxes();
		for i in 0..(box_length - 1) {
			if rng.next_float() < chance_to_change + (i as f64 / box_length as f64 - 0.5) * 2.0 * favour_top {
				// We can change something
				// What to do:
				match rng.next_range(0, 3) {
					0 => {
						// Don't swap, just randomize orientation:
						self.rot_box(i, Orientation::random_orientation(rng)).expect("Failed to shuffle tower");
					}
					1 => {
						// Swap, keep orientation:
						// TODO: Only swapping with items above the current index similar to Fisher–Yates algorithm.
						// Not sure if accurate in this case.
						self.swap_boxes(i, rng.next_range((i + 1) as i64, box_length as i64) as usize);
					}
					2 => {
						// Swap, and then randomize orientation of the new element at i
						self.swap_boxes(i, rng.next_range((i + 1) as i64, box_length as i64) as usize);
						self.rot_box(i, Orientation::random_orientation(rng)).expect("Failed to shuffle tower");
					}
					_ => { unreachable!() }
				}
			}
		}
		// Update the height variable, important because the private fns swap_boxes and rot_box don't
		self.update_height();
	}

	/// Update the height variable of the tower.
	/// Ignores any blocks that don't fit (but still continues checking blocks after)
	fn update_height(&mut self) {
		let mut ll = HashSet::new(); // Should be a linked list here, but rust linked list is not that good

		self.height = self.boxes.iter().fold((Face(std::i32::MAX, std::i32::MAX), 0),
		                                     |(last_face, height), c| {
			                                     let new_face = c.face();
			                                     if last_face.does_self_fit(&new_face).0 == Fit::Below {
				                                     (new_face, height + c.height())
			                                     } else {
				                                     ll.insert(c.clone());
				                                     (last_face, height)
			                                     }
		                                     }).1 as usize;
	}

	/// Fetch number of boxes.
	fn num_of_boxes(&self) -> usize {
		self.boxes.len()
	}

	/// Swap boxes of index `a` and `b`.
	/// WARNING: This function is very likely to change the height of the tower,
	///   but no update is invoked. Be sure to run self.update_height once finished
	///   modifying a tower.
	fn swap_boxes(&mut self, a: usize, b: usize) {
		self.boxes.swap(a, b);
	}

	/// Rotate the box at `idx` to `ori`.
	/// WARNING: This function is very likely to change the height of the tower,
	///   but no update is invoked. Be sure to run self.update_height once finished
	///   modifying a tower.
	fn rot_box(&mut self, idx: usize, ori: Orientation) -> Result<(), ()> {
		match self.boxes.get_mut(idx) {
			Some(box_thing) => box_thing.orientation = ori,
			None => return Err(()),
		}
		Ok(())
	}
}

/// Random number generator (not really random but random enough)
#[derive(Debug, Clone)]
pub struct RNG {
	seed: u64
}

impl RNG {
	pub fn new(seed: u64) -> RNG {
		RNG { seed }
	}

	pub fn next(&mut self) -> u64 {
		self.seed ^= self.seed << 21;
		self.seed ^= self.seed >> 35;
		self.seed ^= self.seed << 4;
		self.seed
	}

	pub fn next_range(&mut self, min: i64, max: i64) -> i64 {
		((self.next() % (max - min) as u64) as i64 + min)
	}

	pub fn next_float(&mut self) -> f64 {
		self.next_range(0, 1_000_000_000) as f64 / 1_000_000_000_f64
	}
}

/// Data send types for sending progress reports back to the main thread
#[derive(Debug)]
pub enum SendData {
	AnnealingTuningComplete(super::annealing_lib::AnnealingParameters),
	AnnealingProgressUpdate(f64),
	AnnealingResult(Tower),
	GeneticResult(Tower),
	GeneticProgressUpdate(f64),
}
