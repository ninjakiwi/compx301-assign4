# COMPX301 Assignment 4

Fourth group assignment for COMPX301

Written in rust, following the specifications
from the [assignment instructions](https://www.cs.waikato.ac.nz/~tcs/COMPX301/assign4-2019.html).

# Authors
 - Daniel Martin - 1349779
 - Rex Pan - 1318909

# In This Project

### Binaries

 - `src/NPStack.rs` *(np_stack)* - Main program

# How To Use

This code was compiled during development with the latest stable release of
rust - `rustc 1.34.1 (2019-04-24)`, though it should work with any other version of rust that
supports the 2018 edition.

To compile the binary (`src/NPStack.rs`) use cargo
which will organize dependencies and link them.

```
$cargo build --release
```

The compiled binary will be in `target/release/`.

### Input File

The input file should have three positive numbers per line, representing a box as "width length height".
If numbers are missing, a value cannot be passed as a int or a value is negative, the line will be skipped.
Anything after the third value on a line will be ignored.

### Arguments

The `np_stack` program can take four arguments only in this order.

> np_stack [-s] <path_to_boxes> <max_candidates> [<optional_integer_seed>]

 - *Silence* (`-s`) - Use a `-s` to silence progress of the algorithms, the output will just be the boxes.
 - *Boxes File* - First argument, the file containing boxes to sort. One box per line in the format "width length height".
 - *Maximum Candidates* - A number specifying the maximum number of candidates we can try.
 - *Seed* (Optional) - The seed to use for randomization, uses system clock if unset.

### Example

To search for the tallest stack from boxes in `text.txt` with a maximum number
of candidates as `1000000`, navigate to `target/release/` and run the binary:

```
$cd target/release/
$./np_stack text.txt 1000000
```

To search for the tallest stack from boxes in `text.txt` with a maximum number
of candidates as `1000000`, a seed of 1234 and silenced progress, run the binary like this:

```
$./np_stack -s text.txt 1000000 1234
```

---
Max candidates = 1000000 takes a couple seconds, it generally gets the best results for time taken.

# Output

The output of `np_stack` is a list of boxes forming the tallest found stack.
One line per box consisting of three integers, width, length and height.

The output blocks are rotated to stack nicely so the numbers may be in a different order then how they were provided.
