//! Handy stuff for printing progress on the command line.
//! 
//! # Authors
//! 
//!  - Daniel Martin - 1349779
//!  - Rex Pan - 1318909

const PART_BARS: [char; 8] = [' ', '▏', '▎', '▍', '▌', '▋', '▊', '▉'];

pub fn generate_loading_bar(amount_complete: f64, total_length: usize) -> String {
	let amount_complete = amount_complete.max(0.0).min(1.0);
	let whole_width = (amount_complete * total_length as f64) as usize;

	let remainder = (amount_complete * total_length as f64).fract();

	if total_length as i32 - whole_width as i32 - 1 < 0 {
		"█".repeat(whole_width)
	} else {
		"█".repeat(whole_width) + &PART_BARS[(remainder * PART_BARS.len() as f64) as usize].to_string()
			+ &" ".repeat(total_length - whole_width - 1)
	}
}

pub enum PhaseProgress {
	Tuning(f64),
	Estimating(f64)
}