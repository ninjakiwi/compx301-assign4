//! Library for all simulated annealing related things.
//! 
//! Simulated annealing algorithm to find a better way to stack blocks on one another.
//! 
//! # Authors
//! 
//!  - Daniel Martin - 1349779
//!  - Rex Pan - 1318909
//! 
//! ## Structs:
//! 
//!  - `AnnealingParameters` - Options for running the annealing stacker.
//! 
//! ## Functions:
//! 
//!  - `annealing` - Run the genetic stacker.
//!  - `switch_state_probabilities` - Get chance to switch selected tower. 

use crate::misc::{Tower, RNG, SendData};
use std::sync::mpsc::Sender;

#[derive(Debug, Default, Copy, Clone)]
pub struct AnnealingParameters {
	pub base_chance_to_change: f64,
	pub potential_power: i32,
//	pub chance_to_switch_at_same_height: f64,
}

pub fn annealing(initial_tower: Tower,
                 max_candi: usize,
                 params: &AnnealingParameters,
                 rng: &mut RNG,
                 progress_updater: Option<&Sender<SendData>>) -> Tower {
	let mut tower_store = {
		[initial_tower.clone(), initial_tower]
	};

	let (current_tower, candi_tower) = tower_store.split_at_mut(1);
	let (mut current_tower, mut candi_tower) = (&mut current_tower[0], &mut candi_tower[0]);

	// For every candidate we can make
	for candi_num in 0..max_candi {
		// Shuffle the tower, shuffle less as we near the last candidates
		candi_tower.shuffle(
			params.base_chance_to_change - params.base_chance_to_change * (candi_num as f64 / max_candi as f64),
			(candi_num as f64 / max_candi as f64) * 0.1,
			rng,
		);

		// Decide if we will switch to the newly created tower, more likely the better the improvement
		// And more likely to pick better towers the closer to the last candidate
		if rng.next_float() <
			switch_state_probabilities(
				current_tower.height(),
				candi_tower.height(),
				candi_num as f64 / max_candi as f64,
				params) {
			std::mem::swap(&mut current_tower, &mut candi_tower);
		}

		*candi_tower = current_tower.clone();

		// Update the main thread on progress
		if candi_num % 10000 == 0 {
			if let Some(updater) = progress_updater {
				updater.send(SendData::AnnealingProgressUpdate(candi_num as f64 / max_candi as f64)).unwrap();
			}
		}
	}

	// TODO: Shouldn't need a clone here, but since we are cloning like 1000000+ times anyway it doesn't really matter
	current_tower.clone()
}

/// Get the chance to switch to a better tower, decided by the difference in height, progress through candidates and static parameters.
fn switch_state_probabilities(height_current: usize, height_potential: usize, progress: f64, params: &AnnealingParameters) -> f64 {
	(height_potential as f64 / height_current as f64).powi((progress * params.potential_power as f64 + 1.0).powi(2) as i32)
		- 0.1
}