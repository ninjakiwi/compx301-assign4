//! Program to find good way to stack blocks.
//! 
//! Runs a genetic algorithm and a simulated annealing algorithm on two
//! different threads, then prints the best result out of the two.
//! 
//! # Authors
//! 
//!  - Daniel Martin - 1349779
//!  - Rex Pan - 1318909
//! 
//! ## Functions:
//! 
//!  - `main` - Run the program.

mod misc;
mod genetic_lib;
mod annealing_lib;
mod cli_helpers;

use genetic_lib::GenerationOptions;
use annealing_lib::AnnealingParameters;
use misc::*;
use cli_helpers::PhaseProgress;

use std::sync::mpsc;
use std::io::{BufRead, Write};
use std::time::{SystemTime, Duration};

/// Milliseconds between each time the task display will switch
const DISPLAY_DELAY_SECONDS: u64 = 1;
/// Fraction of how many of the candidates simulated annealing will use to tune it's values
const TUNING_FRAC: f64 = 1.0 / 6.0;
/// Default annealing values
const DEFAULT_ANNEALING_PARAMS: AnnealingParameters = AnnealingParameters {
	base_chance_to_change: 0.5,
	potential_power: 30,
};
/// Minimum candidates to do tuning
const MINIMUM_REQUIRED_TUNING_CANDI: usize = 500;

fn main() {

	let args = if let Ok(args) = parse_arguments() {
		args
	} else {
		eprintln!("Invalid input - Usage: np_stack [-s] <path_to_boxes> <max_candidates> [<optional_integer_seed>]");
		return;
	};

	let max_candi = args.max_candi;
	let mut boxes = args.boxes;

	// Get rng number if it exists (default to truncated system time as microseconds)
	let mut rng = RNG::new(args.seed);

	// Create another random (one for each task)
	let mut rng2 = RNG::new(args.seed);

	// Randomize start tower
	boxes.shuffle(1.0, 0.0, &mut rng);

	// Setup channel for cross thread comms
	let (annealing_sender, receiver) = mpsc::channel();
	let genetic_sender = annealing_sender.clone();

	// Annealing thread
	std::thread::spawn({
		let boxes = boxes.clone();
		let show_progress = args.show_progress;
		const TUNING_RUNS: usize = 18 * 13;
		move || {
			// Tune the annealing values
			let mut best = 0;
			let mut annealing_params = AnnealingParameters::default();
			let mut best_annealing_params = DEFAULT_ANNEALING_PARAMS;

			let tuning_candi = (max_candi as f64 * TUNING_FRAC / TUNING_RUNS as f64) as usize;
			if tuning_candi > MINIMUM_REQUIRED_TUNING_CANDI {
				for i in 2..20 {
					annealing_params.base_chance_to_change = i as f64 * 0.05;
					for i in (16..=40).step_by(2) {
						annealing_params.potential_power = i;
						let current = annealing_lib::annealing(boxes.clone(),
						                                       tuning_candi,
						                                       &annealing_params,
						                                       &mut rng,
						                                       None).height();
						if current > best {
							best = current;
							best_annealing_params = annealing_params;
						}
					}

					if show_progress {
						annealing_sender.send(
							SendData::AnnealingProgressUpdate((i - 2) as f64 / 18.0)).unwrap();
					}
				}
			}
			annealing_sender.send(SendData::AnnealingTuningComplete(best_annealing_params))
				.expect("Failed to send from annealing thread");

			// Run the actual annealing fn with the decided parameters
			let res_tower = annealing_lib::annealing(boxes,
			                                         (max_candi as f64 * if tuning_candi > MINIMUM_REQUIRED_TUNING_CANDI { (1.0 - TUNING_FRAC) } else { 1.0 }) as usize,
			                                         &best_annealing_params,
			                                         &mut rng,
			                                         if show_progress {Some(&annealing_sender)} else {None});

			annealing_sender.send(SendData::AnnealingResult(res_tower)).unwrap();
		}
	});

	// Genetic thread
	std::thread::spawn({
		let show_progress = args.show_progress;
		move || {
		// Create generation options (keep, merge, mutate lowest chance, mutate highest chance)
		let gen_ops = GenerationOptions::new(5, 8, 0.0, 1.0);
		// Run genetic algorithm and send result
		let res_tower = genetic_lib::genetic(boxes, max_candi / 3, gen_ops, &mut rng2, if show_progress {Some(&genetic_sender)} else {None});
		genetic_sender.send(SendData::GeneticResult(res_tower)).unwrap();
	}});

	// Init all values used for the progress display and storing result output

	let mut annealing_progress = PhaseProgress::Tuning(0.0);
	let mut genetic_progress = PhaseProgress::Estimating(0.0);
	const LOADING_PHASES: [char; 4] = ['-', '\\', '|', '/'];
	let mut current_phase = 0;

	let mut best_annealing_tower = None;
	let mut best_genetic_tower = None;
	let mut task1 = true; // Task display toggle
	let mut partly_completed = false; // If one of the algorithms has finished
	let mut start_time = SystemTime::now();
	let mut current_time;

	// While algorithms are still running
	while best_genetic_tower.is_none() || best_annealing_tower.is_none() {

		if args.show_progress {
			current_time = SystemTime::now();

			if partly_completed {
				task1 = best_annealing_tower.is_none();
			} else if current_time.duration_since(start_time)
				.expect("Failed to get time")
				.as_secs() >=
				DISPLAY_DELAY_SECONDS {
				// Time up! switch displayed task and reset timer
				task1 = !task1;
				start_time = current_time;
			}

			// Switch task shown every second, only print if still working.
			if task1 {
				print!("\r[({0}),  {1} ]", LOADING_PHASES[current_phase],
					if !partly_completed { LOADING_PHASES[current_phase] } else { '✓' });

				match annealing_progress {
					PhaseProgress::Tuning(progress) => {
						print!(" Annealing Tuning Progress:      [{}]  {:.1}% ",
							cli_helpers::generate_loading_bar(progress, 15),
							progress * 100.0);
					}
					PhaseProgress::Estimating(progress) if progress < 1.0 => {
						print!(" Annealing Estimating Progress:  [{}]  {:.1}% ",
							cli_helpers::generate_loading_bar(progress, 15),
							progress * 100.0);
					}
					_ => {}
				}
			} else {
				print!("\r[ {1} , ({0})]", LOADING_PHASES[current_phase],
					if !partly_completed { LOADING_PHASES[current_phase] } else { '✓' });

				match genetic_progress {
					PhaseProgress::Tuning(progress) => {
						print!(" Genetic Tuning Progress:        [{}]  {:.1}% ",
							cli_helpers::generate_loading_bar(progress, 15),
							progress * 100.0);
					}
					PhaseProgress::Estimating(progress) if progress < 1.0 => {
						print!(" Genetic Estimating Progress:    [{}]  {:.1}% ",
							cli_helpers::generate_loading_bar(progress, 15),
							progress * 100.0);
					}
					_ => {}
				}
			}

			// Flush stdio and switch the loading phase
			std::io::stdout().flush().unwrap();
			current_phase = (current_phase + 1) % LOADING_PHASES.len();
		}

		// Check for any data send from one of the tasks
		if let Ok(data) = receiver.recv_timeout(Duration::from_millis(100)) {
			match data {
				// Annealing tuning complete
				SendData::AnnealingTuningComplete(params) => {
					annealing_progress = PhaseProgress::Estimating(0.0);
					if args.show_progress {
						println!("\n{:#?}", params);
					}
				}

				// Progress sent, update progress variable
				SendData::AnnealingProgressUpdate(progress) => {
					match annealing_progress {
						PhaseProgress::Estimating(ref mut prog) => *prog = progress,
						PhaseProgress::Tuning(ref mut prog) => *prog = progress,
					}
				}
				SendData::GeneticProgressUpdate(progress) => {
					match genetic_progress {
						PhaseProgress::Estimating(ref mut prog) => *prog = progress,
						PhaseProgress::Tuning(ref mut prog) => *prog = progress,
					}
				}

				// Result sent, save best tower
				SendData::AnnealingResult(tower) => {
					best_annealing_tower = Some(tower);
					partly_completed = true;
				}
				SendData::GeneticResult(tower) => {
					best_genetic_tower = Some(tower);
					partly_completed = true;
				}
			}
		}
	}

	let ann_tower = best_annealing_tower.expect("Failed to retrieve best annealing tower");
	let gen_tower = best_genetic_tower.expect("Failed to retrieve best genetic tower");

	if args.show_progress {
		// Print out height of both towers
		println!("\n\nResults:");
		println!("    Best Annealing Tower Height: {}", ann_tower.height());
		println!("    Best Genetic Tower Height: {}", gen_tower.height());
	}

	// Pick the highest tower and print it and it's height
	if ann_tower.height() > gen_tower.height() {
		if args.show_progress {println!("Annealing Tower:")};
		ann_tower.dump();
		println!("{}", ann_tower.height());
	} else {
		if args.show_progress {println!("Genetic Tower:")};
		gen_tower.dump();
		println!("{}", gen_tower.height());
	}
}

#[derive(Debug)]
struct CmdArguments {
	show_progress: bool,
	boxes: Tower,
	seed: u64,
	max_candi: usize,
}

fn parse_arguments() -> Result<CmdArguments, ()> {
	let mut current_arg = 1;

	let show_progress = match std::env::args().nth(current_arg) {
		None => return Err(()),
		Some(ref arg) if arg == &"-s".to_owned() => {
			current_arg += 1;
			false
		}
		Some(_) => {
			true
		},
	};

	// Fetch boxes line by line from file
	let mut tower_builder: TowerBuilder = TowerBuilder::new();
	for line in std::io::BufReader::new(
		std::fs::File::open(
			std::env::args().nth(current_arg).ok_or(())?)
			.expect("Cannot open file at given path."))
		.lines() {
		tower_builder.push_from_string(line.expect("Expected string, invalid file input")).err(); // Ignore error (skip bad lines)
	}
	let boxes = tower_builder.build();

	current_arg += 1;

	// Fetch max candidate solutions
	let max_candi = std::env::args()
		.nth(current_arg).ok_or(())?
		.parse::<usize>().map_err(|_| ())?;

	current_arg += 1;

	let seed = std::env::args()
		.nth(current_arg)
		.map_or(
			SystemTime::now()
				.duration_since(SystemTime::UNIX_EPOCH)
				.expect("SystemTime before UNIX EPOCH!")
				.as_micros() as u64, // Default
			|v| v.parse::<u64>().expect("Seed given but not a number"));

	Ok(CmdArguments { show_progress, boxes, max_candi, seed })
}
