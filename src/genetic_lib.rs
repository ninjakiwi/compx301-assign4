//! Library for all genetic related things.
//!
//! Genetic algorithm to find a better way to stack blocks on one another.
//!
//! # Authors
//!
//!  - Daniel Martin - 1349779
//!  - Rex Pan - 1318909
//!
//! ## Structs:
//!
//!  - `GeneticOptions` - Options for running the genetic stacker.
//!
//! ## Functions:
//!
//!  - `genetic` - Run the genetic stacker.

use std::sync::mpsc::Sender;
use crate::misc::{Tower, RNG, SendData};

/// Dictates the frequency of progress updates to the main thread. (candidates per update)
const GENERATIONS_PER_UPDATE: isize = 200;

/// Runs the genetic algorithm.
///
/// ## Args:
///
///  - `boxes` - An array of boxes to find a better stack for.
///  - `max_candi` - The maximum number of candidates we are allowed to produce.
///  - `random` - RNG to use.
///  - `progress_updater` - If exists, well send progress updates through this sender.
///
/// ## Returns:
///
/// Returns the best `Tower` that it found.
pub fn genetic(
	tower: Tower,
	max_candi: usize,
	gen_ops: GenerationOptions,
	random: &mut RNG,
	progress_updater: Option<&Sender<SendData>>) -> Tower {

	// Get size of each generation from options
	let gen_size = gen_ops.generation_size();

	// If we don't have enough space for one generation, return default
	if max_candi < gen_size {
		return tower;
	}

	// Create the first generation (complete random)
	let mut generation = Generation::new_random_generation(&tower, gen_ops, random);

	// Counter so we don't send updates too often
	let mut update_count: isize = 1;

	// Number of generations we can do without exceeding the candidates limit
	let generations = max_candi / gen_size;

	// Create generation after generation
	for i in 0..generations {
		generation.next_generation(random);

		// Remove a generation's worth of candidates from update
		update_count -= gen_size as isize;
		// Send progress and reset `update_count` if down to 0
		if update_count <= 0 {
			if let Some(updater) = progress_updater {
				update_count = GENERATIONS_PER_UPDATE;
				updater.send(SendData::GeneticProgressUpdate(i as f64 / generations as f64))
					.expect("Failed to send from genetic thread");
			}
		}
	}

	generation.best().clone()
}

/// Struct to store the options for running the genetic algorithm.
///
/// ## Values:
///
///  - `keep` - Number of best towers to keep per generation.
///  - `merge` - Number of towers to create by merging previous towers.
///  - `lower_rng` - Lowest possible chance for a tower to be randomly modified.
///  - `upper_rng` - Highest possible chance for a tower to be randomly modified.
#[derive(Debug, Default)]
pub struct GenerationOptions {
	keep: usize,
	merge: usize,
	lower_rng: f64,
	upper_rng: f64,
}

impl GenerationOptions {
	pub fn new(keep: usize, merge: usize, lower_rng: f64, upper_rng: f64) -> GenerationOptions {
		GenerationOptions {
			keep,
			merge,
			lower_rng,
			upper_rng,
		}
	}

	/// Returns the full size of a population.
	pub fn generation_size(&self) -> usize {
		self.keep + self.merge * self.merge
	}
}

/// A generation, capable of turning itself into it's next generation.
///
/// ## Values:
///
///  - `towers` - A vector containing all the towers, normally in order from best to worst height.
///  - `options` - This generation's `GenerationOptions`.
#[derive(Debug)]
struct Generation {
	towers_store: [Vec<Tower>; 2],
	active_tower_at_0: bool,
	options: GenerationOptions,
}

impl Generation {
	/// Create a new generation with the given boxes and options, randomise the order with RNG.
	fn new_random_generation(boxes: &Tower, gen_ops: GenerationOptions, random: &mut RNG) -> Generation {

		// Create a new generation with the correct vector size and generation options
		let gen_size = gen_ops.generation_size();
		let mut new_generation = Generation {
			towers_store: [Vec::with_capacity(gen_ops.generation_size()),
				Vec::with_capacity(gen_ops.generation_size())],
			active_tower_at_0: true,
			options: gen_ops,
		};

		// Fill the generation with random variants of the given tower
		for _ in 0..gen_size {
			new_generation.towers_store[0].push(boxes.clone_shuffled(random));
		}

		// Sort towers by height and return the new generation
		new_generation.towers_store[0].sort_by(Tower::sound_compare);
		new_generation
	}

	/// Generate the next generation using the generation options and the given RNG.
	fn next_generation(&mut self, rng: &mut RNG) {
		// Create a new empty tower vec for the new generation
//		let mut towers = Vec::with_capacity(self.towers.len());
		let (a, b) = self.towers_store.split_at_mut(1);
		let (active_towers, new_towers) = {
			if self.active_tower_at_0 {
				(&mut a[0], &mut b[0])
			} else {
				(&mut b[0], &mut a[0])
			}
		};
//		let active_towers = &self.towers_store.split_at_mut(1)[self.active_tower];
//		let new_towers = &mut self.towers_store[self.active_tower ^ 1];
		new_towers.clear();
		// Add the best (as many as `self.options.keep`) towers to the new vector
		for c in active_towers.iter().take(self.options.keep) {
			new_towers.push(c.clone());
		}

		// Fill the rest of the new generation with merges of towers
		for tower in active_towers.iter()
			.take(self.options.merge / 4)
			.cycle()
			.zip(active_towers.iter().skip(1).take(self.options.merge)) {
			new_towers.push(tower.0.merge(tower.1, rng));
		}

		for i in 0..active_towers.len().min(self.options.merge) {
			for i2 in i + 1..active_towers.len().min(self.options.merge + i + 1) {
				new_towers.push(active_towers[i].merge(&active_towers[i2], rng));
			}
		}

		// Sort the towers by height and randomize/mutate the not so good ones
		new_towers.sort_by(Tower::sound_compare);
		for (i, c) in new_towers.iter_mut().skip(self.options.keep).enumerate() {
			// Randomize tower, gets more random to lower on the list the tower is
			// The min and max randomness are specified in the generation options
			c.shuffle(
				(self.options.upper_rng - self.options.lower_rng)
					* (i as f64
					/ (active_towers.len() - self.options.keep) as f64)
					+ self.options.lower_rng,
				0.0,
				rng,
			);
		}

		// Finally sort the towers and replace the old towers with the new
		new_towers.sort_by(Tower::sound_compare);
		self.active_tower_at_0 = !self.active_tower_at_0;
	}

	/// Get this generation's best tower.
	fn best(&self) -> &Tower {
		&self.towers_store[if self.active_tower_at_0 { 0 } else { 1 }][0]
	}
}
